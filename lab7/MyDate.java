import java.util.Date;

public class MyDate {
    int day, month, year;

    int [] maxDays = {31,29,31,30,31,30,31,31,30,31,30,31};


    public MyDate(int day, int month, int year) {

        this.day = day;
        this.month = month;
        this.year = year;

    }

    @Override
    public String toString() {
        return year + "-" + (month <10 ? "0" : "") + month + "-" + (day <10 ? "0" : "") + day; //year+"" yaparak string yaptık.
    }


    public void incrementDay() {
        day++;
        if (day> maxDays[month-1]){
            day = 1;
            incrementMonth();
        }else if (month == 2 && day == 29 && !inLeapYear()){
            day = 1;
            month++;
        }
    }

    public boolean inLeapYear(){
        return year % 4 == 0;
    }

    public void incrementYear(int diff) {
        year += diff;
        if (month == 2 && day == 29 && !inLeapYear()){
            day = 28;
        }
    }

    public void decrementDay() {                                //?
        day--;
        if (day == 0){
            month--;
            if (!inLeapYear() && month == 2)
                day = 28;
            else {
                day = maxDays[month - 1];
            }
        }

/*decrementMonth();
            if (!inLeapYear() && month == 2)
                day = 28;
            else{
                day = maxDays[month -1];
            }*/


    }

    public void decrementYear() {
        year--;
        if (!inLeapYear() && month == 2)
            day = 28;
        if (inLeapYear() && month == 2)
            day = 29;


        //incrementYear(-1);
    }

    public void decrementMonth() {
        decrementMonth(1);
    }

    public void incrementDay(int diff) {
        while (diff > 0){
            incrementDay();
            diff--;
        }
    }

    public void decrementMonth(int diff) {
        incrementMonth(-diff);
    }

    public void incrementMonth(int diff) {
        month += diff;
        int yearDiff = (month-1) / 12;


        int newMonth = ((month-1) % 12 ) + 1;

        if (newMonth > 0)
            yearDiff--;
        year += yearDiff;
        month = newMonth < 0 ? newMonth + 12 : newMonth;

        if (day > maxDays[month-1]){
            day = maxDays[month-1];
            if (month ==2 && day == 29 && !inLeapYear()){
                day = 28;
            }
        }
    }

    public void decrementDay(int diff) {
        while (diff > 0){
            decrementDay();
            diff--;
        }
    }

    public void decrementYear (int diff){
        incrementYear(-diff);                     //?????  -1
    }

    public void incrementMonth() {
        incrementMonth(1);
    }

    public void incrementYear (){
        incrementYear(1);

    }


    public boolean isBefore(MyDate Date) {
        boolean check;
        if (year < Date.year){
            check = true;
        }else if (year == Date.year && month < Date.month){
            check = true;
        }else if (year == Date.year && month == Date.month && day< Date.day){
            check = true;
        }else{
            check = false;
        }
        return check;
    }



    public boolean isAfter(MyDate Date) {
        boolean check;
        if (year > Date.year){
            check = true;
        }else if (year == Date.year && month > Date.month){
            check = true;
        }else if (year == Date.year && month == Date.month && day> Date.day){
            check = true;
        }else{
            check = false;
        }
        return check;
    }



    public int dayDifference(MyDate Date) {
        int dayDifference1;
        if (!isBefore(Date) && !inLeapYear()){
            Date.maxDays[1] = 28;
            dayDifference1 = Date.maxDays[Date.month - 1]- Date.day + day;
        }else {
            maxDays[1] = inLeapYear() ? 28 : 29;
            dayDifference1 = (maxDays[month - 1] - day) + Date.day;
        }
        return dayDifference1;
    }
}