package shapes;

public class Square extends Shape{

    private double side;

    public Square(double side){
        super();
        this.side = side;
    }

    @Override
    public double area() {
        return Math.pow(side,2);
    }
}
