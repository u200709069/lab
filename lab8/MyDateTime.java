public class MyDateTime extends Object{

    MyDate date;
    MyTime time;

    public MyDateTime(MyDate date, MyTime time) {
        this.date = date;
        this.time = time;
    }

     public String toString(){
        return date + " " + time;

    }

    public void incrementDay(){
        date.incrementDay();
    }

    public void incrementHour(){
        incrementHour(1);
    }

    public void incrementHour(int diff) {
        int dayDiff = time.incrementHour(diff);
        if (dayDiff < 0)
            date.decrementDay(-dayDiff);
        else{
            date.incrementDay(dayDiff);
        }

    }

    public void decrementHour(int diff) {
        incrementHour(-diff);
    }

    public void incrementMinute(int diff) {
        int dayDiff = time.incrementMinute(diff);
        if (dayDiff < 0)
            date.decrementDay(-dayDiff);
        else{
            date.incrementDay(dayDiff);
        }
    }

    public void decrementMinute(int diff) {
        incrementMinute(-diff);
    }

    public void incrementYear(int diff) {
        date.incrementYear(diff);
    }

    public void decrementDay() {
        date.decrementDay();
    }

    public void decrementYear() {
        date.decrementYear();
    }

    public void decrementMonth() {
        date.decrementMonth();
    }

    public void incrementDay(int i) {
        date.incrementDay(i);
    }


    public void decrementMonth(int i) {
        date.decrementMonth(i);
    }

    public void decrementDay(int i) {
        date.decrementDay(i);
    }

    public void incrementMonth(int i) {
        date.incrementMonth(i);
    }

    public void decrementYear(int i) {
        date.decrementYear(i);
    }

    public void incrementMonth() {
        date.incrementMonth();
    }

    public void incrementYear() {
        date.incrementYear();
    }

    public boolean isBefore(MyDateTime anotherDateTime) {
        boolean check;
        if (date.year < anotherDateTime.date.year){
            check = false;
        }else if (date.year == anotherDateTime.date.year && date.month < anotherDateTime.date.month){
            check = false;
        }else if (date.year == anotherDateTime.date.year && date.month == anotherDateTime.date.month
                && date.day< anotherDateTime.date.day){
            check = false;
        }else{
            check = true;
        }
        return check;
    }


    public boolean isAfter(MyDateTime anotherDateTime) {
        boolean check;
        if (date.year > anotherDateTime.date.year){
            check = true;
        }else if (date.year == anotherDateTime.date.year && date.month > anotherDateTime.date.month){
            check = true;
        }else if (date.year == anotherDateTime.date.year && date.month == anotherDateTime.date.month
                && date.day> anotherDateTime.date.day){
            check = true;
        }else{
            check = false;
        }
        return check;
    }
}
